# Learn Vue

> A Vue.js project with Vuex and vue-router

## Build Setup

``` bash
# install dependencies
yarn # or npm install

# serve with hot reload at localhost:8080
yarn dev # or npm run dev

# build for production with minification
yarn build # or npm run build

# build for production and view the bundle analyzer report
yarn build --report # or npm run build --report
```
