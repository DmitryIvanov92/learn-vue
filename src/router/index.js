import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import Native from '@/pages/Native'
import Vuejs from '@/pages/Vuejs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/native/:id',
      name: 'Native',
      component: Native
    },
    {
      path: '/vue/:id',
      name: 'Vuejs',
      component: Vuejs
    }
  ]
})
